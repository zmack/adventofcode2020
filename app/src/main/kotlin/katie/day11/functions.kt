package katie.day11

import java.io.File
import java.time.format.SignStyle

typealias SeatMap = List<CharArray>
typealias Direction = Pair<Int, Int>


class SightlineSeatMap() {
    fun adjacencyList(seatMap: SeatMap, rowIndex: Int, columnIndex: Int): List<Char> {
        val list = mutableListOf<Char>()
        val rowRange = seatMap.indices
        val columnRange = seatMap.first().indices
        (-1..1).forEach line@{ rowDelta ->
            if (!rowRange.contains(rowIndex + rowDelta)) {
                return@line
            }

            (-1..1).forEach column@{ columnDelta ->
                if (rowDelta == 0 && columnDelta == 0) {
                    return@column
                }

                firstVisibleSeatInDirection(seatMap,
                    rowIndex + rowDelta,
                    columnIndex + columnDelta,
                    Direction(rowDelta, columnDelta))?.let { list.add(it) }
            }
        }

        return list
    }
    private fun seatShouldBeVacated(seatMap: SeatMap, rowIndex: Int, columnIndex: Int): Boolean =
        adjacencyList(seatMap, rowIndex, columnIndex).filter { it == '#' }.size >= 5

    private fun seatShouldBeFilled(seatMap: SeatMap, rowIndex: Int, columnIndex: Int): Boolean =
        adjacencyList(seatMap, rowIndex, columnIndex).all { it == 'L' || it == '.' }

    private fun stateForChair(seatMap: SeatMap, rowIndex: Int, columnIndex: Int): Char =
        when (seatMap[rowIndex][columnIndex]) {
            '#' -> if (seatShouldBeVacated(seatMap, rowIndex, columnIndex)) 'L' else '#'
            'L' -> if (seatShouldBeFilled(seatMap, rowIndex, columnIndex)) '#' else 'L'
            else -> seatMap[rowIndex][columnIndex]
        }

    fun iterateSeatMap(seatMap: SeatMap): SeatMap {
        var newSeatMap = List(seatMap.size) { CharArray(seatMap.firstOrNull()?.size ?: 0) }
        seatMap.forEachIndexed { rowIndex, line ->
            line.forEachIndexed { columnIndex, _ ->
                newSeatMap[rowIndex][columnIndex] = stateForChair(seatMap, rowIndex, columnIndex)
            }
        }
        return newSeatMap
    }
}

fun firstVisibleSeatInDirection(seatMap: SeatMap, rowIndex: Int, columnIndex: Int, direction: Direction): Char? {
    val rowRange = seatMap.indices
    val columnRange = seatMap.first().indices
    val (rowDelta, columnDelta) = direction

    if (!rowRange.contains(rowIndex) || !columnRange.contains(columnIndex)) {
        return null
    }

    val currentPoint = seatMap[rowIndex][columnIndex]

    if (currentPoint == 'L' || currentPoint == '#') {
        return currentPoint
    } else {
        return firstVisibleSeatInDirection(seatMap, rowIndex + rowDelta, columnIndex + columnDelta, direction)
    }
}


fun adjacencyList(seatMap: SeatMap, rowIndex: Int, columnIndex: Int): List<Char> {
    val list = mutableListOf<Char>()
    val rowRange = seatMap.indices
    val columnRange = seatMap.first().indices
    (-1..1).forEach line@{ rowDelta ->
        if (!rowRange.contains(rowIndex + rowDelta)) {
            return@line
        }

        (-1..1).forEach column@{ columnDelta ->
            if (rowDelta == 0 && columnDelta == 0) {
                return@column
            }

            if (!columnRange.contains(columnIndex + columnDelta)) {
                return@column
            }

            list.add(seatMap[rowIndex + rowDelta][columnIndex + columnDelta])
        }
    }

    return list
}

fun seatShouldBeVacated(seatMap: SeatMap, rowIndex: Int, columnIndex: Int): Boolean =
    adjacencyList(seatMap, rowIndex, columnIndex).filter { it == '#' }.size >= 4

fun seatShouldBeFilled(seatMap: SeatMap, rowIndex: Int, columnIndex: Int): Boolean =
    adjacencyList(seatMap, rowIndex, columnIndex).all { it == 'L' || it == '.' }

fun stateForChair(seatMap: SeatMap, rowIndex: Int, columnIndex: Int): Char =
    when (seatMap[rowIndex][columnIndex]) {
        '#' -> if (seatShouldBeVacated(seatMap, rowIndex, columnIndex)) 'L' else '#'
        'L' -> if (seatShouldBeFilled(seatMap, rowIndex, columnIndex)) '#' else 'L'
        else -> seatMap[rowIndex][columnIndex]
    }

fun iterateSeatMap(seatMap: SeatMap): SeatMap {
    var newSeatMap = List(seatMap.size) { CharArray(seatMap.firstOrNull()?.size ?: 0) }
    seatMap.forEachIndexed { rowIndex, line ->
        line.forEachIndexed { columnIndex, _ ->
            newSeatMap[rowIndex][columnIndex] = stateForChair(seatMap, rowIndex, columnIndex)
        }
    }
    return newSeatMap
}

fun printSeatMap(seatMap: SeatMap) = seatMap.forEach { line -> println(line.joinToString())}


fun run() {
    val seatMap = File("/Users/zmack/projects/katie/inputs/day11.txt").readLines().map { it.toCharArray() }

    var map = SightlineSeatMap()

    var oldState = map.iterateSeatMap(seatMap)

    while (true) {
        val newState = map.iterateSeatMap(oldState)
        val newStateString = newState.joinToString { it.joinToString() }
        val oldStateString = oldState.joinToString { it.joinToString() }

        oldState = newState
        if ( newStateString == oldStateString ) {
            println("Map stabilized")
            break
        }
    }

    val occupiedSeats = oldState.map { it.count { char -> char == '#'} }.sum()
    println("Found $occupiedSeats occupied seats")
}
