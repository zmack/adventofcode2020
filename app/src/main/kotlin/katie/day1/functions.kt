package katie.day1

import java.io.File

fun findTwo(expenses: List<Int>, totalAmount: Int = 2020): Pair<Int, Int>? {
    expenses.forEachIndexed block@{ index, element ->
        val iterator = expenses.listIterator(index + 1)
        if (!iterator.hasNext()) return null

        val matchingNumber = iterator.asSequence().find { element + it == totalAmount }
        if (matchingNumber == null) return@block

        return Pair(element, matchingNumber)
    }

    return null
}

fun findThree(expenses: List<Int>): Triple<Int, Int, Int>? {
    val listLength = expenses.size
    expenses.forEachIndexed block@{ index, element ->
        val pair = findTwo(expenses.subList(index + 1, listLength), 2020 - element)
        if (pair != null) {
            return Triple(element, pair.first, pair.second)
        }
    }

    return null
}

fun run() {
    val expenses = File("/Users/zmack/projects/katie/inputs/day1.txt")
            .useLines { it.toList() }
            .map{ it.toInt() }

    val triple = findThree(expenses)
    if (triple != null) {
        println(triple)
        println(triple.component1() * triple.component2() * triple.component3())
    }
}