package katie.day12

import java.io.File
import java.lang.Math.abs

class Ship {
    // positive latitude -> North, negative -> South
    var latitude = 0
    // positive longitude -> East, negative -> West
    var longitude = 0

    // latitude delta, longitude delta
    var heading = Pair(0, 1)

    fun moveNorth(units: Int) {
        latitude += units
    }

    fun moveSouth(units: Int) {
        latitude -= units
    }

    fun moveEast(units: Int) {
        longitude += units
    }

    fun moveWest(units: Int) {
        longitude -= units
    }

    fun turn(degrees: Int) {
        when (degrees) {
            90, -270 -> {
                heading = if (heading.first == 0) {
                    Pair(heading.second, heading.first)
                } else {
                    Pair(-1 * heading.second, -1 * heading.first)
                }
            }
            180, -180 -> {
                heading = Pair(heading.first * -1, heading.second * -1)
            }
            270, -90 -> {
                heading = if (heading.first == 0) {
                    Pair(-1 * heading.second, -1 * heading.first)
                } else {
                    Pair(heading.second, heading.first)
                }
            }
            360 -> {}
            else -> throw(Error("Unexpected invocation $degrees"))
        }
    }

    fun ahead(units: Int) {
        latitude += heading.first * units
        longitude += heading.second * units
    }

    fun move(motion: String) {
        val amount = motion.substring(1).toInt()
        when (motion.first()) {
            'F' -> ahead(amount)
            'N' -> moveNorth(amount)
            'S' -> moveSouth(amount)
            'E' -> moveEast(amount)
            'W' -> moveWest(amount)
            'L' -> turn(amount)
            'R' -> turn(-amount)
        }
    }

    fun travel(route: List<String>) {
        route.forEach { move(it) }
    }

    fun manhattanDistance(): Int =
        abs(latitude) + abs(longitude)
}

fun run() {
    val travelData = File("/Users/zmack/projects/katie/inputs/day12.txt").readLines()

    val ship = Ship()
    ship.travel(travelData)
    println("Hello ${ship.latitude} ${ship.longitude} ${ship.manhattanDistance()}")
}
