package katie.day13

import java.io.File
import kotlin.math.abs


fun run() {
    val travelData = File("/Users/zmack/projects/katie/inputs/day13.txt").readLines()
    val departureTime = travelData.first().toInt()
    val busLines = travelData[1].split(",").filter { it != "x" }.map { it.toLong() }
    val timeOffsets: Map<Long, Long> = travelData[1].split(",")
        .mapIndexed { index, item -> if (item != "x") Pair(item.toLong(), index.toLong()) else null }
        .filterNotNull()
        .let { println(it); it }
        .map { (key, value) -> Pair(key, abs(key - value) % key) }
        .toMap()

    val remainders: Map<Long, Long> = travelData[1].split(",")
        .mapIndexed { index, item -> if (item != "x") Pair(item.toLong(), index.toLong()) else null }
        .filterNotNull()
        .toMap()

    println(timeOffsets)

    var step = timeOffsets.keys.max()!!
    var currentNumber = timeOffsets[step]!!
    val factorsAccountedFor = mutableSetOf(step)
    println("Starting at $currentNumber with $step")

    while (!remainders.all { (key, value) -> ( currentNumber + value ) % key == 0L }) {
        val newFactors = remainders.filter { (key, value) -> ( currentNumber + value ) % key == 0L }.keys
        (newFactors - factorsAccountedFor).forEach {
            step *= it
            factorsAccountedFor.add(it)
            println("Step is $step $factorsAccountedFor")
        }
        currentNumber += step
    }

    println("The number is $currentNumber")

}
