package katie.day5

import java.io.File
import java.util.*

data class SeatAssignment(val row: Byte, val seat: Byte) {
    fun seatId() : Int = row * 8 + seat
}

fun seatCodeToAssignment(seatCode: String) : SeatAssignment {
    val rowNumber = BitSet(7)
    val seatNumber = BitSet(3)
    seatCode.forEachIndexed { index, char ->
        when (char) {
            'B' -> rowNumber.set(7 - index - 1)
            'R' -> seatNumber.set(10 - index - 1)
        }
    }

    return SeatAssignment(
        rowNumber.toByteArray().getOrElse(0) { value -> value.toByte() },
        seatNumber.toByteArray().getOrElse(0) { value -> value.toByte() }
    )
}

fun run() {
    val seatData = File("/Users/zmack/projects/katie/inputs/day5.txt")
        .useLines { it.toList() }
    val seatAssignments = seatData.map { seatCodeToAssignment(it) }.sortedBy { it.seatId() }

    (seatAssignments.first().seatId()..seatAssignments.last().seatId()).forEachIndexed { index, seatId ->
        if (seatAssignments[index].seatId() != seatId ) {
            println("Seat missing for $seatId")
            return
        }
    }

    println(seatData.map { seatCodeToAssignment(it).seatId() }.sorted())
}
