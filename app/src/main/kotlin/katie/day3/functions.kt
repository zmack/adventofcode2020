package katie.day3

import java.io.File

fun isTree(line: CharArray, column: Int) : Boolean {
    return line[column % line.size] == '#'
}

fun treesOnPath(map: List<CharArray>, column_offset: Int = 3, line_offset: Int = 1) : Int =
    map.filterIndexed iter@{ index, line ->
        if (index % line_offset != 0 ) return@iter false
        isTree(line, index / line_offset * column_offset)
    }.count()


fun run() {
    val map = File("/Users/zmack/projects/katie/inputs/day3.txt")
        .useLines { it.toList() }
        .map{ it.toCharArray() }

    val treeCounts = listOf(Pair(1,1), Pair(3,1), Pair(5,1), Pair(7,1), Pair(1,2)).map { (column_offset, line_offset) ->
        treesOnPath(map, column_offset, line_offset)
    }

    val initial : Long = 1
    val product = treeCounts.fold(initial, { acc, number -> acc * number })
    println("Product of $treeCounts is $product")
}