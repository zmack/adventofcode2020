package katie.day8

import java.io.File

class Console(private val memory: List<String>,
              private var acc: Int = 0,
              private var ip: Int = 0,
              private var ipValues: MutableSet<Int> = mutableSetOf()
) {

    private fun reset() {
        acc = 0; ip = 0; ipValues = mutableSetOf()
    }

    fun run() : Pair<Boolean, Int> {
        reset()
        while(ip < memory.size) {
            if (ipValues.contains(ip)) {
                return Pair(false, acc)
            } else {
                ipValues.add(ip)
            }
            process(memory[ip])
        }

        return Pair(true, acc)
    }

    fun runPatched(patch: (Int, String) -> String ) : Pair<Boolean, Int> {
        reset()
        while(ip < memory.size) {
            if (ipValues.contains(ip)) {
                return Pair(false, acc)
            } else {
                ipValues.add(ip)
            }
            process(patch(ip, memory[ip]))
        }

        return Pair(true, acc)
    }

    private fun process(instructionLine: String) {
        val (instruction, arg) = instructionLine.split(' ')
        when (instruction) {
            "acc" -> { acc += arg.toInt(); ip++ }
            "nop" -> { ip++ }
            "jmp" -> { ip += arg.toInt() }
        }
    }
}

fun run() {
    val instructionData = File("/Users/zmack/projects/katie/inputs/day8.txt").readLines()
    val console = Console(instructionData)

    instructionData.forEachIndexed iter@{ index, line ->
        val (instruction, args) = line.split(' ')

        val (returned, acc) = when (instruction) {
            "nop" -> {
                console.runPatched { ip, currentLine -> if (ip == index) "jmp $args" else currentLine }
            }
            "jmp" -> {
                console.runPatched { ip, currentLine -> if (ip == index) "nop $args" else currentLine }
            }
            else -> return@iter
        }

        if (returned) {
            println("acc was $acc")
        }
    }

    println(console.run())
}
