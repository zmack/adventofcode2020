package katie.day2

import katie.day1.findThree
import java.io.File
import java.lang.IndexOutOfBoundsException

data class PasswordPolicy(val letter: Char, val repetitionRange: IntRange) {
    fun passwordComplies(password: String):Boolean {
        val repetitions = password.filter { it == letter }.length
        return repetitionRange.contains(repetitions)
    }

    fun passwordCompliesToToboggan(password: String):Boolean {
        return try {
            (password[repetitionRange.first - 1] == letter) xor (password[repetitionRange.last - 1] == letter)
        } catch (e: IndexOutOfBoundsException) {
            false
        }
    }
}

fun specToPolicy(spec: String) : PasswordPolicy {
    val splitList = spec.split(" ")

    val letter:Char = splitList[1][0]
    val range = splitList[0]
    val limits = range.split("-")

    return PasswordPolicy(letter, IntRange(limits[0].toInt(), limits[1].toInt()))
}

fun passwordLineValid(line: String) : Boolean {
    val splitLine = line.split(": ")

    val policy = specToPolicy(splitLine[0])
    val password = splitLine[1]

    return policy.passwordCompliesToToboggan(password)
}

fun run() {
    val passwords = File("/Users/zmack/projects/katie/inputs/day2.txt")
            .useLines { it.toList() }

    val validPasswordCount = passwords.listIterator().asSequence().filter { passwordLineValid(it) }.count()
    print("$validPasswordCount valid passwords found")
}