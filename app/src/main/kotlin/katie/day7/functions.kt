package katie.day7

import java.io.File

class NoMatchFound: Exception("No match found")

data class BagData(val quantity: Int, val color: String) {
    companion object {
        private val specMatcher = Regex("(\\d+) (.*) bags?")

        fun fromString(string: String) : BagData {
            val match = specMatcher.matchEntire(string) ?: throw NoMatchFound()
            return BagData(match.groupValues[1].toInt(), match.groupValues[2])
        }
    }
}

data class BagRule(val color: String, val includedColors: List<BagData>) {
    companion object {
        private val bagMatcher = Regex("(.*) bags? contain (\\d+ .*,?|no other bags)+\\.")

        fun fromString(line: String) : BagRule {
            val match = bagMatcher.matchEntire(line) ?: throw NoMatchFound()
            val bagColor = match.groupValues[1]

            val containedBags = if (match.groupValues[2] != "no other bags") {
                match.groupValues[2].split(", ").map { BagData.fromString(it) }
            } else {
                emptyList()
            }
            return BagRule(bagColor, containedBags)
        }
    }
}

fun accumulateBags(key: String, accumulator: MutableSet<String>, bagMap: Map<String, List<String>>) {
    bagMap[key]?.forEach {
        accumulateBags(it, accumulator, bagMap)
        accumulator.add(it)
    }
}

fun countBags(key: String, bagRules: Map<String, BagRule>) : Long {
    val bagRule = bagRules[key] ?: return 1
    return if (bagRule.includedColors.isNullOrEmpty()) {
        1
    } else {
        println(bagRule.includedColors)
        1 + bagRule.includedColors.map { it.quantity * countBags(it.color, bagRules) }.sum()
    }
}

fun run() {
    val customsData = File("/Users/zmack/projects/katie/inputs/day7.txt").readLines()
    val bagRules = customsData.map { val bagRule = BagRule.fromString(it); Pair(bagRule.color, bagRule) }.toMap()
    val bagInclusion: MutableMap<String, MutableList<String>> = mutableMapOf()

    bagRules.values.forEach {
        it.includedColors.forEach { bag ->
            val colorList: MutableList<String> = bagInclusion.getOrPut(bag.color, { ArrayList() })
            colorList.add(it.color)
        }
    }

    val availableBags: MutableSet<String> = HashSet()

    accumulateBags("shiny gold", availableBags, bagInclusion)
    val bagCount = countBags("shiny gold", bagRules)

    println(availableBags.size)
    // Remove one bag because that's our guy
    println(bagCount - 1)
}

