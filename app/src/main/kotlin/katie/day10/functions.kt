package katie.day10

import java.io.File
import kotlin.math.max


fun distributionData(joltageData: List<Int>): Triple<Int, Int, Int> {
    val (distribution, _) = joltageData.fold(Pair(Triple(0, 0, 0), 0)) {
            (distribution, last_number), i ->
        val (ones, twos, threes) = distribution
        when (i - last_number) {
            1 -> Pair(Triple(ones + 1, twos, threes), i)
            2 -> Pair(Triple(ones, twos + 1, threes), i)
            3 -> Pair(Triple(ones, twos, threes + 1), i)
            else -> throw(Error("Unknown diff $last_number $i"))
        }
    }

    return distribution
}

fun consecutiveGroups(joltageData: List<Int>): List<List<Int>> {
    var groups = mutableListOf<List<Int>>()
    joltageData.foldIndexed(emptyList<Int>()) { index, list, i ->
        val lastNumber = list.lastOrNull() ?: 0
        when (i - lastNumber) {
            1 -> {
                if (index == joltageData.size - 1) {
                    groups.add(list + i)
                }
                list + i
            }
            3 -> {
                groups.add(list)
                listOf(i)
            }
            else -> throw(Error("Unknown diff ${i - lastNumber}"))
        }
    }
    return groups
}

fun fanSpinner(joltageData: kotlin.collections.List<Int>,
               min: Int = joltageData.firstOrNull() ?: 0,
               max: Int = joltageData.lastOrNull() ?: 0,
               delta: Int = 3) : Long {
    return joltageData.foldIndexed(0L) { index, acc, number ->
        if (number - min > delta) {
            return acc
        }

        if (number == max) {
            acc + 1
        } else {
            acc + fanSpinner(joltageData.subList(index + 1, joltageData.size).toList(), min = number, delta = delta)
        }
    }
}

fun chattyFanSpinner(joltageData: List<Int>,
                     currentList: List<Int>,
                     min: Int = joltageData.firstOrNull() ?: 0,
                     max: Int = joltageData.lastOrNull() ?: 0,
                     delta: Int = 3) : Long {
    return joltageData.foldIndexed(0L) { index, acc, number ->
        if (number - min > delta) {
            return acc
        }

        if (number == max) {
            // println("--> $number $min ${currentList + number}")
            acc + 1
        } else {
            val list = currentList + number
            acc + chattyFanSpinner(
                joltageData.subList(index + 1, joltageData.size).toList(),
                min = number,
                currentList = list,
                delta = delta)
        }
    }
}

/*
2, 3, 6
3, 6
2, 3, 6
1, 2, 3, 6
1, 3, 6
 */

fun run() {
    val joltageData = File("/Users/zmack/projects/katie/inputs/day10.txt").readLines().map { it.toInt() }.sorted()
    val distribution = distributionData(joltageData)
    val groups = consecutiveGroups(joltageData)

/*
    println("Running big chatty $joltageData")
    val total_options = chattyFanSpinner(joltageData, currentList = listOf(0), min = 0)
    println("Big Chattty -> $total_options")
 */

    var options = chattyFanSpinner(groups.first(), currentList = listOf(0), min = 0)

    groups.subList(1, groups.size).forEach {
        val currentGroupOptions = chattyFanSpinner(it.subList(1, it.size).toList(),
            min = it.first(),
            currentList = listOf(it.first()))
        println("Group $it -> $currentGroupOptions")
        options *= max(currentGroupOptions, 1)
    }
    println("Got $options")
    println(groups)

    println("Total ${joltageData.size} Distribution is $distribution, Number is ${distribution.first * (distribution.third + 1)}")
}
