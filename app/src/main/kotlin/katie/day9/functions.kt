package katie.day9

import java.io.File

class XMASHack(private val numbers: List<Long>) {
    fun findWeakSequence(vulnerableNumber: Long) : List<Long> {
        var sum = 0L
        val buffer = mutableListOf(*numbers.takeWhile {
            if (sum + it < vulnerableNumber) {
                sum += it; true
            } else false
        }.toTypedArray())

        numbers.subList(buffer.size, numbers.size).forEach {
            while (sum + it > vulnerableNumber) {
                sum -= buffer[0]
                buffer.removeAt(0)
            }

            if (sum + it == vulnerableNumber) {
                buffer.add(it)
                return buffer
            }

            sum += it
            buffer.add(it)
        }
        return buffer
    }
}

class XMASValidator(private val numbers: List<Long>) {

    private fun validNumber(buffer: List<Long>, number: Long) : Boolean {
        return buffer.filterIndexed { index, current ->
            buffer.subList(index + 1, buffer.size).any { it + current == number }
        }.any()
    }

    fun weakNumber(prefixCount: Int = 25) : Long? {
        val buffer = mutableListOf(*numbers.take(prefixCount).toTypedArray())
        val message = numbers.subList(prefixCount, numbers.size)

        message.forEach {
            if (!validNumber(buffer, it)) {
                return it
            }

            buffer.removeAt(0)
            buffer.add(it)
        }
        return null
    }

}

fun run() {
    val xmasNumbers = File("/Users/zmack/projects/katie/inputs/day9.txt").readLines().map { it.toLong() }

    val vulnerableNumber = XMASValidator(xmasNumbers).weakNumber(25)!!
    println(vulnerableNumber)
    val sequence = XMASHack(xmasNumbers).findWeakSequence(vulnerableNumber)
    println("${sequence.sum()} $vulnerableNumber ${sequence.min()!! + sequence.max()!!}")

}

