package katie.day6

import java.io.File
import kotlin.streams.toList

class Group(private val answers: MutableList<String> = mutableListOf()) {
    fun addAnswer(answer: String) = answers.add(answer)

    fun numAffirmativeAnswers() = answers.flatMap { it.chars().toList() }.distinct().count()

    fun numCommonAffirmativeAnswers() = answers.map { it.chars().toList() }.reduce { acc, answers ->
        acc.intersect(answers).toList()
    }.size
}

fun run() {
    val customsData = File("/Users/zmack/projects/katie/inputs/day6.txt").readLines()

    val groups = customsData.fold(listOf(Group()), { acc, line ->
        if (line.isBlank()) {
            acc + Group()
        } else {
            val group = acc.last()
            group.addAnswer(line)
            acc
        }
    })

    val totalAffirmativeAnswers = groups.map { it.numAffirmativeAnswers() }.sum()
    println("Total $totalAffirmativeAnswers affirmative answers out of ${groups.size} groups")

    val totalCommonAffirmativeAnswers = groups.map { it.numCommonAffirmativeAnswers() }.sum()
    println("Total $totalCommonAffirmativeAnswers common affirmative answers")
}