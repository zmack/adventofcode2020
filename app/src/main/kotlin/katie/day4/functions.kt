package katie.day4

import java.io.File

data class Passport(
    var byr: String? = null,
    var iyr: String? = null,
    var eyr: String? = null,
    var hgt: String? = null,
    var hcl: String? = null,
    var ecl: String? = null,
    var pid: String? = null,
    var cid: String? = null
) {
    fun update(line: String) {
        val values = line.split(' ')
                    .map { it.split(':')}

        values.forEach { (key, value) ->
            when (key) {
                "byr" -> this.byr = value
                "iyr" -> this.iyr = value
                "eyr" -> this.eyr = value
                "hgt" -> this.hgt = value
                "hcl" -> this.hcl = value
                "ecl" -> this.ecl = value
                "pid" -> this.pid = value
                "cid" -> this.cid = value
            }
        }
    }

    fun isPresent() : Boolean {
        return !(this.byr.isNullOrBlank() ||
                this.iyr.isNullOrBlank() ||
                this.eyr.isNullOrBlank() ||
                this.hgt.isNullOrBlank() ||
                this.hcl.isNullOrBlank() ||
                this.ecl.isNullOrBlank() ||
                this.pid.isNullOrBlank())
    }

    fun isValid() : Boolean {
        return isValidBirthYear() &&
                isValidIssueYear() &&
                isValidExpirationYear() &&
                isValidHairColor() &&
                isValidEyeColor() &&
                isValidPassportId() &&
                isValidHeight()
    }

    private fun isValidYear(year: String, range: IntRange) : Boolean = range.contains(year.toInt())

    private fun isValidBirthYear() : Boolean = isValidYear(this.byr ?: "0", 1920..2002)

    private fun isValidIssueYear() : Boolean = isValidYear(this.iyr ?: "0", 2010..2020)

    private fun isValidExpirationYear() : Boolean = isValidYear(this.eyr ?: "0", 2020..2030)

    private fun isValidHairColor() : Boolean = this.hcl?.matches(Regex("#[0-9a-f]{6}")) ?: false

    private fun isValidEyeColor() : Boolean = this.ecl?.matches(Regex("(amb|blu|brn|gry|grn|hzl|oth)")) ?: false

    private fun isValidPassportId() : Boolean = this.pid?.matches(Regex("\\d{9}")) ?: false

    private fun isValidHeight() : Boolean {
        val expression = Regex("(\\d+)(cm|in)")
        val matchResult = expression.matchEntire(this.hgt ?: "") ?: return false

        val height = matchResult.groupValues[1].toInt()
        val units = matchResult.groupValues[2]

        return when (units) {
            "cm" -> (150..193).contains(height)
            "in" -> (59..76).contains(height)
            else -> false
        }
    }
}

fun run() {
    val passportData = File("/Users/zmack/projects/katie/inputs/day4.txt")
        .useLines { it.toList() }
        .fold(listOf(Passport()), { acc, line ->
            if (line.length == 0) {
                acc + Passport()
            } else {
                acc.last().update(line)
                acc
            }
        })

    println("${passportData.filter { it.isPresent() && it.isValid() }.count()} valid passports")
}
