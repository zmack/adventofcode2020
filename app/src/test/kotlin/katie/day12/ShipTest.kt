package katie.day12

import org.junit.Test
import kotlin.test.assertEquals

internal class ShipTest {
    @Test fun `turning the ship works`() {
        val ship = Ship()

        val initialHeading = ship.heading

        ship.turn(-90)
        ship.turn(-90)
        ship.turn(-90)
        ship.turn(-90)
        assertEquals(initialHeading, ship.heading)


        ship.turn(90)
        ship.turn(90)
        ship.turn(90)
        ship.turn(90)
        assertEquals(initialHeading, ship.heading)

        ship.turn(270)
        ship.turn(90)
        assertEquals(initialHeading, ship.heading)

        ship.turn(-270)
        ship.turn(-90)
        assertEquals(initialHeading, ship.heading)
    }
}
