package katie.day11

import org.junit.Test
import kotlin.test.assertEquals

internal class FunctionsTest {
    private val testMap =
        """
            L.LL.LL.LL
            LLLLLLL.LL
            L.L.L..L..
            LLLL.LL.LL
            L.LL.LL.LL
            L.LLLLL.LL
            ..L.L.....
            LLLLLLLLLL
            L.LLLLLL.L
            L.LLLLL.LL
        """.trimIndent().lines().map { it.toCharArray() }

    @Test fun `can get a top left adjacency list`() {
        val list = adjacencyList(testMap, 0, 0)
        assertEquals(charArrayOf('.', 'L', 'L').asList(), list)
    }

    @Test fun `can get a top middle adjacency list`() {
        val list = adjacencyList(testMap, 0,1)
        assertEquals(charArrayOf('L', 'L', 'L', 'L', 'L').asList(), list)
    }

    @Test fun `can get a top right adjacency list`() {
        val list = adjacencyList(testMap, 0,9)
        assertEquals(charArrayOf('L', 'L', 'L').asList(), list)
    }

    @Test fun `can get a bottom left adjacency list`() {
        val list = adjacencyList(testMap, 9,0)
        assertEquals(charArrayOf('L', '.', '.').asList(), list)
    }

    @Test fun `can get a bottom middle adjacency list`() {
        val list = adjacencyList(testMap, 9,1)
        assertEquals(charArrayOf('L', '.', 'L', 'L', 'L').asList(), list)
    }

    @Test fun `can get a bottom right adjacency list`() {
        val list = adjacencyList(testMap, 9,9)
        assertEquals(charArrayOf('.', 'L', 'L').asList(), list)
    }

    @Test fun `first iteration works`() {
        val processedMap =
            """
                #.##.##.##
                #######.##
                #.#.#..#..
                ####.##.##
                #.##.##.##
                #.#####.##
                ..#.#.....
                ##########
                #.######.#
                #.#####.##
            """.trimIndent().lines().map { it.toCharArray() }

        val actual = iterateSeatMap(testMap).joinToString { it.joinToString() }
        assertEquals(processedMap.joinToString { it.joinToString() }, actual)
    }

    @Test fun `sightline adjusted adjacency list works`() {
        val sightLineTestMap = """
            .......#.
            ...#.....
            .#.......
            .........
            ..#L....#
            ....#....
            .........
            #........
            ...#.....
        """.trimIndent().lines().map { it.toCharArray() }

        val map = SightlineSeatMap()

        assertEquals("########", map.adjacencyList(sightLineTestMap, 4, 3).joinToString(""))

        assertEquals("L", map.adjacencyList(sightLineTestMap, 4, 8).joinToString(""))
    }
}