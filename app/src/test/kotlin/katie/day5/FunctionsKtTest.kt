package katie.day5

import org.junit.Test
import kotlin.test.assertEquals

internal class FunctionsKtTest {
    private val seatAssignmentTestValues = mapOf(
        "BFFFBBFRRR" to SeatAssignment(70, 7),
        "FFFBBBFRRR" to SeatAssignment(14, 7),
        "BBFFBBFRLL" to SeatAssignment(102, 4),
        "BBBBBBBRRR" to SeatAssignment(127, 7),
        "FFFFFFFLLL" to SeatAssignment(0, 0)
    )

    private val seatIdTestValues = mapOf(
        "BFFFBBFRRR" to 567,
        "FFFBBBFRRR" to 119,
        "BBFFBBFRLL" to 820
    )

    @Test fun `can compute seat assignments`() {
        seatAssignmentTestValues.forEach { (seatCode, seatAssignment) ->
            assertEquals(seatAssignment, seatCodeToAssignment(seatCode))
        }
    }

    @Test fun `can compute seat ids`() {
        seatIdTestValues.forEach { (seatCode, seatAssignment) ->
            assertEquals(seatAssignment, seatCodeToAssignment(seatCode).seatId())
        }
    }

}